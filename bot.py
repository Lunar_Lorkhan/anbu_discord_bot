import discord
import json
from ANBU_discord_bot import Parser
from ANBU_discord_bot.Download_file import download_file as df
from ANBU_discord_bot.Add_player_from_discord import add_player

VERSION = "1.2"

AUTHOR = "Lunar_Lorkhan"

Release_date = "21.06.2020"

PATH_NOTES = "С этой версии бот стал юзабельным для просмотра краткой статистики"


class MyClient(discord.Client, discord.User):
    async def on_ready(self):
        print('Logged on as {0}!'.format(self.user))
        await self.change_presence(status=discord.Status.online, activity=discord.Game("+help"))

    async def on_message(self, message):
        if message.author == self.user:
            return
        else:
            channel = message.channel
            if message.content.startswith('+stat'):
                # if message.attachments:
                #     url = message.attachments[0].url
                #     df(url)
                # else:
                #     embed_obj = create_message_embed('Отсутствует аргумент',
                #                                      'Загрузите файл.')
                #     await channel.send(embed=embed_obj)
                #     return
                pasports_player_dict, cv_session_list = Parser.parser('combat.log')
                players_from_discord = get_nickname_from_discord()
                try:
                    player_nickname = players_from_discord[message.author.id]
                except (KeyError, TypeError):
                    embed_obj = create_stat_embed(cv_session_list, pasports_player_dict, nickname=None)
                    await channel.send(embed=embed_obj)
                    return
                embed_obj = create_stat_embed(cv_session_list, pasports_player_dict, nickname=player_nickname)
                await channel.send(embed=embed_obj)
            elif message.content.startswith('+help'):
                help_embed = create_help_embed()
                await channel.send(embed=help_embed)
            elif message.content.startswith('+reg'):
                mes = message.content.split(' ')
                if len(mes) == 2:
                    add_player(message.author.id, mes[1])
                    embed_obj = create_message_embed('Регистрация выполнена успешно',
                                                     'Теперь вы можете получать краткую статистику')
                    await channel.send(embed=embed_obj)
                else:
                    embed_obj = create_message_embed('Неверные аргументы',
                                                     'Введите никнейм корректно')
                    await channel.send(embed=embed_obj)
            elif message.content.startswith('+test'):
                print(message.author.id)
                print(message.content)
                a = message.content
                a = a.split(' ')
                print(a)


def get_nickname_from_discord():
    with open("discord_players.json", "r") as f:
        try:
            data = json.load(f)
        except json.JSONDecodeError:
            return
        return {int(x): y for x in data.keys() for y in data.values()}


def create_stat_embed(cv_session_list: list, pasports_player_dict: dict, nickname=None):  # warning! Сложный код
    print('Создание ембеда для статистики...')
    info = ''
    embed_obj = discord.Embed(colour=discord.Colour.green())
    embed_obj.set_author(name='Подробная статистика', url='')
    cv_count = len(cv_session_list)
    for i in pasports_player_dict:
        player = pasports_player_dict[i]
        if player.get_nickname() == nickname and nickname:
            defeat_count = 0
            uid_player = player.get_uid()
            player_cv_list = player.get_cv_sessions()
            for cv_session in player_cv_list:
                team_1, team_2 = cv_session.get_cv_player_list()
                if cv_session.get_team_win() != get_team(team_1, team_2, uid_player):
                    defeat_count += 1
            embed_obj.description = 'Количество кв сессий: {}. Победы/Поражения: {}/{}'.format(cv_count,
                                                                                               cv_count - defeat_count,
                                                                                               defeat_count)
            for teammate in get_player_list(player, pasports_player_dict):
                info += teammate.get_info()
            embed_obj.add_field(name='Краткая статистика:',
                                value=info,
                                inline=False)
            embed_obj.add_field(name='Карты:',
                                value=get_info_about_map(cv_session_list))
            break
    else:
        embed_obj.description = 'Количество кв сессий: {}'.format(cv_count)
        embed_obj.add_field(name='Карты:',
                            value=get_info_about_map(cv_session_list))
        embed_obj.add_field(name='Как получить краткую статистику?',
                            value='Пропиши +reg <игровой ник>',
                            inline=False)
    print('Создание ембеда прошло успешно!')
    return embed_obj


def get_info_about_map(cv_session_list):
    map_cv = {}
    info = ''
    for cv in cv_session_list:
        mmap = cv.get_map()
        print(map_cv)
        if mmap not in map_cv:
            map_cv[mmap] = 1
        else:
            map_cv[mmap] += 1
    for i in map_cv:
        info += '{} - {}\n'.format(i, map_cv[i])
    return info


def get_player_list(player, pasports_player_dict) -> list:
    player_list = [player]
    for uid in player.get_teammates():
        teammate = pasports_player_dict[uid]
        player_list.append(teammate)
    player_list.sort(key=lambda x: x.get_damage(), reverse=True)
    return player_list


def get_team(team_1: list, team_2: list, uid: int) -> int:
    for player in team_1:
        if player.get_uid() == uid:
            return player.get_team()
    for player in team_2:
        if player.get_uid() == uid:
            return player.get_team()


def create_help_embed():
    embed_obj = discord.Embed(title='Анализирую кв статистику за ваши bucks.',
                              description='Как пользоваться?',
                              colour=discord.Colour.green())
    embed_obj.set_author(name="Бот альянса ANBU",
                         url='https://discord.gg/7jgcQwQ')
    embed_obj.add_field(name='Стата:',
                        value='+stat <combat.log> - просмотреть стату за кв сессию',
                        inline=False)
    embed_obj.add_field(name='Регистрация:',
                        value='+reg <имя игрока> - зарегистрировать свое имя для получения краткой статистики',
                        inline=False)
    embed_obj.add_field(name='Информация:',
                        value='Версия: {}\nАвтор: {}\nPatch Notes: {}'.format(VERSION,
                                                                              AUTHOR,
                                                                              PATH_NOTES),
                        inline=False)
    return embed_obj


def create_message_embed(title: str, description: str):
    embed_obj = discord.Embed(title=title,
                              description=description,
                              colour=discord.Colour.green())
    embed_obj.set_author(name="Бот альянса ANBU",
                         url='https://discord.gg/7jgcQwQ')
    return embed_obj


client = MyClient()
client.run('Njc4NjA5MjQzNzMzMDk4NTM2.Xu9irw.AKobU3G3yJ4E6TGC7jDmjMQmwQI')
