import re
from ANBU_discord_bot.Session_and_players import Player, Session, Clan_War, Pasport_Player
import json

# Шаблоны для строк
###

# время
pattern_time = r'\d\d\:\d\d\:\d\d.\d{3}'
# [0] - режим игры, [1] - карта
pattern_mode_map = r"(?<=')\w+"
# [0] - номер игрока, [1] - uid, [2] - пачка, [3] - номер команды, [4] - бот, [5] - ur
pattern_player_info = r"\d+(?=,)"
# [0] - никнейм
pattern_nickname = r"(?<=nickname:)[^,:]+"
# [0] - причина
pattern_reason = r"(?<=reason:)[^\|]+"
# [0] - номер игрока
pattern_player = r"(?<=player:)[^,]+"
# [0] - никнейм
pattern_nick = r"(?<=nick:)[^,:]+"
# [0] - кол-во очков
pattern_got = r"(?<=Got:)[^,]+"
# [0] - жертва
pattern_victim = r"(?<=Victim: )[^ ]+"
# [0] - атакующий
pattern_attacker = r"(?<=attacker:)[^,:]+"
# [0] - оружие
pattern_weapon1 = r"(?<=weapon')[^']+"
# [0] - оружие
pattern_weapon2 = r"(?<=weapon:')[^']+"
# [0] - дамаг
pattern_damage = r"(?<=damage:)\d+"
# [0] - киллер
pattern_killer = r"(?<=killer:)[\w+/]+"
# [0] - никнейм
pattern_assist = r"(?<=assist\sby\s)[^\s]+"
# [0] - причина завершения боя, [1] - причина победы
pattern_reason_finish = r'(?<=reason:)[^,\s]+'
# [0] - номер выигравшей команды
pattern_winner_team = r'(?<=team)\d'
# [0] - время катки
pattern_battle_time = r'(?<=time:)\d+'


###

def parser(path: str) -> (dict, list):
    """Парсит файл
    :param path: путь к файл
    :return: pasports_player_dict - словарь с информацией об игроках за все кв сессии {uid: <объект типа Player_Pasport>}
             cv_session_list - список со всеми кв сессиями [<Clan_War>]
    """
    print("Обработка файла...")
    cv_session_list = []  # список всех кв
    round_list = []  # список раундов кв
    pasports_player_dict = {}  # список всех игроков со всех кв {<uid>: <объект типа Pasport_Player>}
    session_player_dict = {}  # список всех игроков за одну сессию кв {<uid>: <объект типа Player>}
    team_1 = {}  # словарь {<номер игрока>: <объект типа Player>}
    team_2 = {}  # словарь {<номер игрока>: <объект типа Player>}
    flag = False  # флаг, показывает, что нашлась сессия кв

    with open(path, 'r') as file:
        for line in file:
            if re.search(r'Gameplay', line) and re.search(r'BestOf3', line):  # начало новой кв сессии
                info = re.findall(pattern_mode_map, line)
                mode = info[0]
                mmap = info[1]
                cv = Clan_War()
                cv.set_map(mmap)
                cv.set_mode(mode)
                cv_session_list.append(cv)
                flag = True
            elif re.search(r'Gameplay finish', line) and flag:  # конец кв сессии
                time = int(re.findall(pattern_battle_time, line.replace(' ', ''))[0])
                winner = int(re.findall(pattern_winner_team, line.replace(' ', ''))[0])
                # reason_finish = re.findall(pattern_reason_finish, line.replace(' ', ''))
                cv.set_time(time)
                cv.set_team_win(winner)
                for i in round_list:
                    cv.append_round(i)
                round_list = []

                pasports_player_dict = update_pasports(pasports_player_dict, session_player_dict, cv)

                session_player_dict = {}
                flag = False
            if flag:
                if re.search(r'levels/maps/hangar', line):  # если игрок покинул игру досрочно
                    session.extend_team_1([i for i in team_1.values()])
                    session.extend_team_2([i for i in team_2.values()])
                    round_list.append(session)
                    for i in round_list:
                        cv.append_round(i)
                    pasports_player_dict = update_pasports(pasports_player_dict, session_player_dict, cv)
                    session_player_dict = {}
                    round_list = []
                    team_1 = {}
                    team_2 = {}
                    flag = False
                if re.search(r'Active battle started', line):
                    team_1 = {}
                    team_2 = {}
                    session = Session()
                elif re.search(r'Best Of N', line):  # конец раунда
                    session.extend_team_1([i for i in team_1.values()])
                    session.extend_team_2([i for i in team_2.values()])

                    time = int(re.findall(pattern_battle_time, line.replace(' ', ''))[0])
                    winner = int(re.findall(pattern_winner_team, line.replace(' ', ''))[0])
                    # reason_finish = re.findall(pattern_reason_finish, line.replace(' ', ''))

                    session.set_time(time)
                    session.set_team_win(winner)
                    round_list.append(session)
                elif re.search(r'mmHash', line):  # добавление игроков в начале раунда
                    nickname = re.findall(pattern_nickname, line.replace(' ', ''))[0]
                    info = re.findall(pattern_player_info, line)
                    team = int(info[3])
                    number = int(info[0])
                    uid = info[1]
                    player = Player()
                    player.set_uid(uid)
                    player.set_nickname(nickname)
                    player.set_team(team)
                    if team == 1:
                        team_1[number] = player
                    else:
                        team_2[number] = player
                    # проверяем, есть ли такой игрок в этой сессии,
                    # если нет, то добавляем этого игрока
                    if uid not in session_player_dict:
                        session_player_dict[uid] = [player]
                    else:
                        session_player_dict[uid].append(player)
                elif re.search(r'Score:', line):  # начисление очков
                    # reason = re.findall(pattern_reason, line.replace(' ', ''))  # может быть больше одного элемента
                    number = int(re.findall(pattern_player, line.replace(' ', ''))[0])
                    score = int(re.findall(pattern_got, line.replace(' ', ''))[0])

                    if number in team_1:
                        player = team_1[number]
                    else:
                        player = team_2[number]
                    player.add_score(score)
                elif re.search(r'Damage\.', line):  # начисление нанесенного урона
                    nickname = re.findall(pattern_attacker, line.replace(' ', ''))[0]
                    victim = re.findall(pattern_victim, line)
                    victim = victim[0].split(":")

                    if len(victim) == 2 or nickname == victim:
                        continue
                    # weapon = re.findall(pattern_weapon1, line.replace(' ', ''))[0]
                    damage = int(re.findall(pattern_damage, line.replace(' ', ''))[0])

                    for player in extend_dict(team_1, team_2):
                        if player.get_nickname() == nickname:
                            player.add_damage(damage)
                elif re.search(r"Kill\.", line):  # начисление убийств и смертей
                    killer_name = re.findall(pattern_killer, line.replace(' ', ''))[0]
                    victim = re.findall(pattern_victim, line)
                    victim = victim[0].split(":")
                    if (len(victim) == 2) or (killer_name == 'n/a') or (victim == killer_name):
                        continue
                    if victim:
                        victim = victim[0]
                    for player in extend_dict(team_1, team_2):
                        if player.get_nickname() == killer_name:
                            player.add_kill()
                    for player in extend_dict(team_1, team_2):
                        if player.get_nickname() == victim:
                            player.set_status(0)
                # elif re.search(r"assist by", line) is not None:
                #     nickname = re.findall(pattern_assist, line)
                #     if nickname:
                #         nickname = nickname[0]
                #     else:
                #         continue
                #     weapon = re.findall(pattern_weapon2, line.replace(' ', ''))[0]
                #     damage = int(re.findall(pattern_damage, line.replace(' ', ''))[0])
                #     if player is not None:  # под вопросом. Непонятно, начисляются ли дамаг за ассисты
                #         pass
    print('Обработка файла завершена успешно!')
    return pasports_player_dict, cv_session_list


def extend_dict(team_1: dict, team_2: dict) -> list:
    """Функция для облегчения обработки списков
    :param team_1: словарь с {<имя игрока>: <объект типа Player>}
    :param team_2: словарь с {<имя игрока>: <объект типа Player>}
    :return: список с объектами типа Player
    """
    list_team_1 = list(team_1.values())
    list_team_2 = list(team_2.values())
    list_team_1.extend(list_team_2)
    return list_team_1


def update_pasports(pasports_player_dict: dict, session_player_dict: dict, cv: Clan_War) -> dict:
    """Добавление информации об игроке в его паспорт
    :param pasports_player_dict: словарь с информацией об игроках {<uid>: <объект типа Player_Pasport>}
    :param session_player_dict: словарь с информацией об игроках за сессию {<uid>: <объект типа Player>}
    :param cv: объект типа <Clan_War>
    :return: pasports_player_dict
    """
    team_1 = []
    team_2 = []
    for i in session_player_dict:
        if i in pasports_player_dict:
            player_list = session_player_dict[i]
            pasport_player = pasports_player_dict[i]
            for player in player_list:
                if player.get_status() == 0:
                    pasport_player.add_death()
                pasport_player.add_kills(player.get_kills())
                pasport_player.add_damage(player.get_damage())
                pasport_player.add_score(player.get_score())
        else:
            player_list = session_player_dict[i]
            pasport_player = Pasport_Player()
            for player in player_list:
                pasport_player.set_uid(player.get_uid())
                pasport_player.set_nickname(player.get_nickname())
                if player.get_status() == 0:
                    pasport_player.add_death()
                pasport_player.add_kills(player.get_kills())
                pasport_player.add_damage(player.get_damage())
                pasport_player.add_score(player.get_score())
            pasports_player_dict[i] = pasport_player
        pasport_player.add_cv_sessions(cv)
        team_number = session_player_dict[i][0].get_team()
        if team_number == 1:
            team_1.append(i)
        else:
            team_2.append(i)
    for i in team_1:
        for j in team_1:
            if i not in pasports_player_dict[j].get_teammates() and i != pasports_player_dict[j].get_uid():
                pasports_player_dict[j].add_teammates(i)
    for i in team_2:
        for j in team_2:
            if i not in pasports_player_dict[j].get_teammates() and i != pasports_player_dict[j].get_uid():
                pasports_player_dict[j].add_teammates(i)
    return pasports_player_dict


def debug(path=r"combat.log") -> None:
    """Функция для дебага парсера
    :param path: путь до файла
    :return: None
    """
    player_list = []
    pasports_player_dict, cv_session_list = parser(path)
    print('Clan war result:')
    for i in cv_session_list:
        print(i)
    print('Pasports player:')
    for i in pasports_player_dict:
        player = pasports_player_dict[i]
        player_list.append(player)
    player_list.sort(key=lambda x: x.get_damage(), reverse=True)
    for i in player_list:
        print(i.get_info())


if __name__ == '__main__':
    debug()
