class Session:
    def __init__(self):
        """Класс, описывающий стандартную сессию(Pve или Pvp матч)
        mmap        - карта <str>
        mode        - режим игры <str>
        time        - время игры <int>
        team_win    - номер команды, которая выиграла <int>
        team_1      - список, в котором хранятся объекты игроков команды 1 этой сессии [Player, ...]
        team_2      - список, в котором хранятся объекты игроков команды 2 этой сессии [Player, ...]
        """
        self.mmap = None
        self.mode = None
        self.time = None
        self.team_win = None
        self.team_1 = []
        self.team_2 = []

    def set_map(self, mmap: str) -> None:
        if isinstance(mmap, str):
            self.mmap = mmap
        else:
            raise ValueError

    def set_mode(self, mode: str) -> None:
        if isinstance(mode, str):
            self.mode = mode
        else:
            raise ValueError

    def set_time(self, time: int) -> None:
        if isinstance(time, int):
            self.time = time
        else:
            raise ValueError

    def set_team_win(self, team_win: int) -> None:
        if isinstance(team_win, int) and (team_win == 1 or team_win == 2):
            self.team_win = team_win
        else:
            raise ValueError

    def extend_team_1(self, team_1: list) -> None:
        self.team_1.extend(team_1)

    def extend_team_2(self, team_2: list) -> None:
        self.team_2.extend(team_2)

    def get_map(self) -> str:
        return self.mmap

    def get_mode(self) -> str:
        return self.mode

    def get_time(self) -> int:
        return self.time

    def get_team_win(self) -> int:
        return self.team_win

    def get_info(self) -> str:
        info = ''
        info += 'time = {}, team_win = {}\n'.format(self.get_time(), self.get_team_win())
        info += 'command 1:\n'
        for player in self.team_1:
            info += player.get_info()
        info += 'command 2:\n'
        for player in self.team_2:
            info += player.get_info()
        return info


class Clan_War:
    def __init__(self):
        """Класс, описывающий сессию кланового боя
        rounds      - Список, в котором хранятся раунды, ссыгранные в данной сессии [Session, ...]
        mode        - режим игры <str>
        mmap        - карта <str>
        time        - общее время игры <int>
        team_win    - победившая команда <int>
        """
        self.rounds = []
        self.mmap = None
        self.mode = None
        self.time = None
        self.team_win = None

    def append_round(self, my_round: Session) -> None:
        if isinstance(my_round, Session):
            self.rounds.append(my_round)
        else:
            raise ValueError

    def set_time(self, time: int) -> None:
        if isinstance(time, int):
            self.time = time
        else:
            raise ValueError

    def set_team_win(self, team_win: int) -> None:
        if isinstance(team_win, int):
            self.team_win = team_win
        else:
            raise ValueError

    def set_map(self, mmap: str) -> None:
        if isinstance(mmap, str):
            self.mmap = mmap
        else:
            raise ValueError

    def set_mode(self, mode: str) -> None:
        if isinstance(mode, str):
            self.mode = mode
        else:
            raise ValueError

    def get_cv_player_list(self) -> tuple:
        if self.rounds:
            session = self.rounds[0]
            team_1 = session.team_1
            team_2 = session.team_2
            return team_1, team_2

    def get_map(self) -> str:
        return self.mmap

    def get_mode(self) -> str:
        return self.mode

    def get_time(self) -> int:
        return self.time

    def get_team_win(self) -> int:
        return self.team_win

    def get_rounds(self) -> list:
        return self.rounds

    def get_info(self):
        info = ''
        i = 0
        for mround in self.rounds:
            i += 1
            info += 'round {}\n'.format(i)
            info += mround.get_info()
        return info

    def __str__(self):
        return f'map {self.get_map()}, mode {self.get_mode()}, time {self.get_time()}, win {self.get_team_win()}\n' \
               + self.get_info()


class Player:
    def __init__(self):
        """Класс, в котором содержится информация об игроке.
        status      - 0-мертв, 1-жив <int>
        kills       - кол-во убийств и ассистов <int>
        uid         - уникальный id игрока <int>
        nickname    - плевдоним игрока <str>
        weapon      - оружия <str>
        achievements- достижения за бой <str>
        damage      - кол-во урона <int>
        team        - команда игрока <int>
        """
        self.status = 1
        self.kills = 0
        self.score = 0
        self.damage = 0
        self.uid = None
        self.nickname = None
        self.weapons = []
        self.achievements = []
        self.team = None

    def set_team(self, team: int) -> None:
        if isinstance(team, int):
            self.team = team
        else:
            raise ValueError

    def set_status(self, status: int) -> None:
        if isinstance(status, int) and (status == 0 or status == 1):
            self.status = status
        else:
            raise ValueError

    def add_kill(self) -> None:
        self.kills += 1

    def add_score(self, score: int) -> None:
        if isinstance(score, int) and score >= 0:
            self.score += score
        else:
            raise ValueError

    def set_uid(self, uid: str) -> None:
        if isinstance(uid, str):
            self.uid = uid
        else:
            raise ValueError

    def set_nickname(self, nickname: str) -> None:
        if isinstance(nickname, str):
            self.nickname = nickname
        else:
            raise ValueError

    def add_damage(self, damage: int) -> None:
        if isinstance(damage, int) and damage >= 0:
            self.damage += damage

    def set_weapon(self):
        pass

    def set_achievement(self):
        pass

    def get_team(self) -> int:
        return self.team

    def get_damage(self) -> int:
        return self.damage

    def get_status(self) -> int:
        return self.status

    def get_kills(self) -> int:
        return self.kills

    def get_score(self) -> int:
        return self.score

    def get_uid(self) -> str:
        return self.uid

    def get_nickname(self) -> str:
        return self.nickname

    def get_weapons(self):
        pass

    def get_achievents(self):
        pass

    def get_info(self):
        return 'status = {}, kills = {}, score = {}, damage = {}, uid = {}, nickname = {}\n'.format(self.get_status(),
                                                                                                    self.get_kills(),
                                                                                                    self.get_score(),
                                                                                                    self.get_damage(),
                                                                                                    self.get_uid(),
                                                                                                    self.get_nickname())


class Pasport_Player(Player):
    """Класс, в котором хранятся данные за все сессии
        kills       - кол-во убийств и ассистов <int>
        uid         - уникальный id игрока <int>
        nickname    - плевдоним игрока <str>
        weapon      - оружия <str>
        achievements- достижения за бой <str>
        damage      - кол-во урона <int>
        teammates   - массив с uid игроков [<str>, ...]
        cv_sessions - массив с сессиями [<Clan_War>, ...]
    """

    def __init__(self):
        super().__init__()
        self.death = 0
        self.teammates = []
        self.cv_sessions = []

    def add_cv_sessions(self, cv: Clan_War) -> None:
        if isinstance(cv, Clan_War):
            self.cv_sessions.append(cv)
        else:
            raise ValueError

    def add_teammates(self, uid: str) -> None:
        if isinstance(uid, str):
            self.teammates.append(uid)
        else:
            raise ValueError

    def add_death(self) -> None:
        self.death += 1

    def add_kills(self, kills: int) -> None:
        if isinstance(kills, int):
            self.kills += kills
        else:
            raise ValueError

    def get_cv_sessions(self) -> list:
        return self.cv_sessions

    def get_death(self) -> int:
        return self.death

    def get_teammates(self) -> list:
        return self.teammates

    def set_team(self, team: int) -> None:
        pass

    def get_team(self) -> None:
        pass

    def set_status(self, status: int) -> None:
        pass

    def add_kill(self) -> None:
        pass

    def get_status(self) -> None:
        return None

    def get_info(self) -> str:
        return 'nickname = {}, kills = {}, death = {} damage = {}, score = {}\n'.format(self.get_nickname(),
                                                                                        self.get_kills(),
                                                                                        self.get_death(),
                                                                                        self.get_damage(),
                                                                                        self.get_score())
