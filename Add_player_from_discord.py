import json
import os.path


def add_player(player_id: int, nickname: str):
    if not os.path.isfile('discord_players.json'):
        with open("discord_players.json", "w") as f:
            pass
    with open("discord_players.json", "r") as f:
        try:
            data = json.load(f)
        except json.JSONDecodeError:
            data = {}
        data[player_id] = nickname
    with open("discord_players.json", "w") as f:
        data[player_id] = nickname
        json.dump(data, f, indent=4)
